﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación
{
    public partial class CIERRE_DE_DÍA : Form
    {
        public CIERRE_DE_DÍA()
        {
            InitializeComponent();
        }

        private void CIERRE_DE_DÍA_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'REPORTERIA.venta_diaria' Puede moverla o quitarla según sea necesario.
           
        }

        private void btn_generar_Click(object sender, EventArgs e)
        {

            DateTime fecha = Convert.ToDateTime(dateTimePicker1.Text);
            this.venta_diariaTableAdapter.Fill(this.REPORTERIA.venta_diaria, fecha);
            // TODO: esta línea de código carga datos en la tabla 'REPORTERIA.egreso' Puede moverla o quitarla según sea necesario.
            this.egresoTableAdapter.Fill(this.REPORTERIA.egreso,fecha);
            // TODO: esta línea de código carga datos en la tabla 'REPORTERIA.EGRESO_INGRESO' Puede moverla o quitarla según sea necesario.
            this.EGRESO_INGRESOTableAdapter.Fill(this.REPORTERIA.EGRESO_INGRESO, fecha, fecha);

            this.reportViewer1.RefreshReport();






        }
    }
}
